
package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static utils.Methods.*;

public class MainPage_Component {
	
	
	public MainPage_Component(WebDriver driver) {
        
        PageFactory.initElements(driver, this);
    }
	
	
	 @FindBy(xpath = "//div[contains(@class,'cbp-logged-user')][contains(text(),'Zalogowany')]")
	 public WebElement lblLogged;
	 
	 @FindBy(xpath = "//div[contains(text(),'Wykonaj')]")
	 public WebElement btnOperations;
	 
	 @FindBy(id = "logout_button")
	 public WebElement btnLogout;
	 
	 
	 
	 
	 public void logout() {
		 
		 clickOn(this.btnLogout);
		 		 
	 }
	
}
